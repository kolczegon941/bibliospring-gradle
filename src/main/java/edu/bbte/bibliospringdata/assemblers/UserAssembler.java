package edu.bbte.bibliospringdata.assemblers;

import edu.bbte.bibliospringdata.dto.incoming.UserInDTO;
import edu.bbte.bibliospringdata.dto.outgoing.UserOutDTO;
import edu.bbte.bibliospringdata.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserAssembler {

    public User userInDtoToModel(UserInDTO userindto) {
        User user = new User();
        user.setUsername(userindto.getUsername());
        user.setPassword(userindto.getPassword());
        return user;
    }

    public UserOutDTO modelToUserOutDto(User user) {
        UserOutDTO userOutDTO = new UserOutDTO();
        userOutDTO.setUuid(user.getUuid());
        userOutDTO.setId(user.getId());
        userOutDTO.setUsername(user.getUsername());
        return userOutDTO;
    }
}
