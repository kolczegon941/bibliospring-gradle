package edu.bbte.bibliospringdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BibliospringdataApplication {

    public static void main(String[] args) {
        SpringApplication.run(BibliospringdataApplication.class, args);
    }

}
