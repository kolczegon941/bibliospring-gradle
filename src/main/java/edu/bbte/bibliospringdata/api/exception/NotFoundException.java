package edu.bbte.bibliospringdata.api.exception;

public class NotFoundException extends RuntimeException {

    private final Class type;

    private final Long id;

    public NotFoundException(Class type, Long id) {
        super();
        this.type = type;
        this.id = id;
    }

    public Class getType() {
        return type;
    }

    public Long getId() {
        return id;
    }
}
