package edu.bbte.bibliospringdata.dto.outgoing;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserOutDTOTest {


    @Test
    void testSetGetId() {

        UserOutDTO user = new UserOutDTO();
        user.setId(100L);

        Long id = user.getId();

        assertEquals(Long.valueOf(100), id, "Failed");
    }

    @Test
    void testSetGetUUID() {

        UserOutDTO user = new UserOutDTO();
        user.setUuid("abc123");

        String uuid = user.getUuid();

        assertEquals("abc123", uuid, "Failed");

    }

    @Test
    void testSetGetUsername() {

        UserOutDTO user = new UserOutDTO();
        user.setUsername("Lajos");

        String username = user.getUsername();

        assertEquals("Lajos", username, "Failed");
    }

}