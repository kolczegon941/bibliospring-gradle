package edu.bbte.bibliospringdata.dto.incoming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserInDTOTest {

    @Test
    void testSetGetUserName() {

        UserInDTO user = new UserInDTO();
        user.setUsername("Bela");

        String username = user.getUsername();

        assertEquals("Bela", username, "Failed");
    }

    @Test
    void testSetGetPassword() {

        UserInDTO user = new UserInDTO();
        user.setPassword("qwerty");

        String password = user.getPassword();

        assertEquals("qwerty", password, "Failed");
    }

}