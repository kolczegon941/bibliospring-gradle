package edu.bbte.bibliospringdata.api;

import edu.bbte.bibliospringdata.assemblers.UserAssembler;
import edu.bbte.bibliospringdata.dto.outgoing.UserOutDTO;
import edu.bbte.bibliospringdata.model.User;
import edu.bbte.bibliospringdata.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserControllerTest {

    @Mock
    private UserService userService;
    @Mock
    private UserAssembler userAssembler;
    @InjectMocks
    private UserController userController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAll() {
        List<User> userList = Arrays.asList(new User(), new User());
        List<UserOutDTO> expectedUserOutDTOList = Arrays.asList(new UserOutDTO(), new UserOutDTO());

        when(userService.getAll()).thenReturn(userList);
        when(userAssembler.modelToUserOutDto(any(User.class))).thenReturn(new UserOutDTO());

        List<UserOutDTO> result = userController.getAll();

        assertEquals(expectedUserOutDTOList.size(), result.size(), "Failed");
        verify(userService, times(1)).getAll();
        verify(userAssembler, times(userList.size())).modelToUserOutDto(any(User.class));
    }

    @Test
    void testGetById() {
        Long userId = 1L;
        User user = new User();
        UserOutDTO expectedUserOutDTO = new UserOutDTO();
        when(userService.getById(userId)).thenReturn(user);
        when(userAssembler.modelToUserOutDto(user)).thenReturn(expectedUserOutDTO);

        UserOutDTO result = userController.getById(userId);

        assertEquals(expectedUserOutDTO, result, "Failed");
        verify(userService, times(1)).getById(userId);
        verify(userAssembler, times(1)).modelToUserOutDto(user);
    }

}