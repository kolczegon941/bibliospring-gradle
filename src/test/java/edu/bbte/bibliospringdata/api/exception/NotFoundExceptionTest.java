package edu.bbte.bibliospringdata.api.exception;

import edu.bbte.bibliospringdata.model.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NotFoundExceptionTest {

    private final Class type = User.class;

    @Test
    void testGetType() {

        Long id = 123L;

        NotFoundException exception = new NotFoundException(type, id);

        assertEquals(type, exception.getType(), "Failed");
    }

    @Test
    void testGetId() {

        Long id = 123L;

        NotFoundException exception = new NotFoundException(type, id);

        assertEquals(id, exception.getId(), "Failed");


    }
}