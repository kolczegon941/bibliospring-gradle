package edu.bbte.bibliospringdata.assemblers;

import edu.bbte.bibliospringdata.dto.incoming.UserInDTO;
import edu.bbte.bibliospringdata.model.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserAssemblerTest {

    @Test
    void testUserInDtoToModelUsername() {

        UserAssembler userAssembler = new UserAssembler();
        UserInDTO userInDTO = new UserInDTO();
        userInDTO.setUsername("testUser");
        userInDTO.setPassword("testPassword");

        User user = userAssembler.userInDtoToModel(userInDTO);

        assertEquals(userInDTO.getUsername(), user.getUsername(), "Failed");

    }

    @Test
    void testUserInDtoToModelPassword() {

        UserAssembler userAssembler = new UserAssembler();
        UserInDTO userInDTO = new UserInDTO();
        userInDTO.setUsername("testUser");
        userInDTO.setPassword("testPassword");

        User user = userAssembler.userInDtoToModel(userInDTO);

        assertEquals(userInDTO.getPassword(), user.getPassword(), "Failed");
    }

}

