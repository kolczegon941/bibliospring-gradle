package edu.bbte.bibliospringdata.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    void testGettersAndSettersUsername() {
        //Arrange
        User user = new User();
        String username = "test";
        //Act
        user.setUsername(username);
        //Assert
        assertEquals(username, user.getUsername(), "username equal");


    }

    @Test
    void testGettersAndSettersPassword() {
        //Arrange
        User user = new User();
        String password = "test";
        //Act
        user.setPassword(password);
        //Assert
        assertEquals(password, user.getPassword(), "password equal");


    }

}